# Getting Started
> Run these all in the root directory of the project folder: 
```bash
cd /home/raydoescode/hack-reactor/projects/unmonolith/conference-4-go/
```

## Process:
1. Build
2. Run

## Monolith
- Build: `docker build -f monolith/Dockerfile.dev ./monolith -t conference-go-dev`
- Run: `docker run -d -v "$(pwd)/monolith:/app" -p "8000:8000" --network conference-go --name monolith conference-go-dev`
- Monitor: `docker container logs --follow monolith`

## Network Bridge
- For **conference-go**: `docker network create --driver bridge conference-go`

## Microservice
- Build: `docker build -f attendees_microservice/Dockerfile.dev . -t attendees-microservice-dev`
- Run: `docker run -d -v "$(pwd)/attendees_microservice:/app" -p "8001:8001" --network conference-go --name attendees-microservice attendees-microservice-dev`
- Monitor: ``

## Clean Up
- Stop: ` docker container stop attendees-microservice monolith`
- Remove Containers: `docker container rm attendees-microservice monolith`

**_Can't Remember:_** `history | grep docker`